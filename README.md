# AngularJS Demo App

Demo application to test out AngularJS and related libraries. Configured to run locally or on a dedicated web server.

## Main Libraries

*If no version mentioned, latest is being used*

* [AngularJS 1.5.x](https://github.com/angular/angular.js)
* [AngularUI Bootstrap 2.5.x](https://github.com/angular-ui/bootstrap)
* [AngularUI Router](https://github.com/angular-ui/ui-router)
* [Bootstrap 3.x](https://github.com/twbs/bootstrap)
* [Webpack 2.x](https://github.com/webpack/webpack)
* [Webpack Dev Server 2.x](https://github.com/webpack/webpack-dev-server)

## Supported Platforms

* [Google Cloud](https://cloud.google.com/)

## External API's Used

* [Unsplash It](https://unsplash.it/)
* [Chuck Norris Database](http://www.icndb.com/)

## Install

Install dependecies using [Yarn](https://yarnpkg.com/en/)

## Commands

`npm run start-dev`

* starts a webpack-dev-server on [http://localhost:8080](http://localhost:8080) referencing files under `src/`

`npm run build`

* bundles application from `src/` into `www/` using webpack

## Google Cloud Deployment

Prior to deploying to [Google Cloud Platform](https://cloud.google.com/), ensure `gcloud` configuration is pointing to personal project and `npm run build` executed successfully. To confirm, verify `www/` directory exists.

To test deployment locally, execute `dev_appserver.py ./` in the root directory. This will host the files under `www/` on [http://localhost:8080](http://localhost:8080). `dev_appserver.py` is an extension of `gcloud`.

To deploy to app engine, execute `gcloud app deploy`. If successful, execute `gcloud app browse` to view application in browser.

## License

[MIT](LICENSE)