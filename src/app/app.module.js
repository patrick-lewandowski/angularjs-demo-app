import angular from 'angular';
import uiRouter from 'angular-ui-router';
import carousel from 'angular-ui-bootstrap/src/carousel';

import routing from './app.routes.js';

require('../assets/css/global.less');

angular.module('app', [ uiRouter, carousel ])
  .config(routing);
