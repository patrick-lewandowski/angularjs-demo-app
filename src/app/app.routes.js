import HomeCtrl from './components/home/home.controller.js';
import AboutCtrl from './components/about/about.controller.js';

routes.$inject = ['$stateProvider', '$locationProvider', '$urlRouterProvider'];

export default function routes($stateProvider, $locationProvider, $urlRouterProvider) {
  $locationProvider
    .hashPrefix('')
    .html5Mode(true);
  $stateProvider
    .state('home', {
      url: '/home',
      template: require('./components/home/home.view.html'),
      controller: HomeCtrl,
      controllerAs: 'home'
    })
    .state('about', {
      url: '/about',
      template: require('./components/about/about.view.html'),
      controller: AboutCtrl,
      controllerAs: 'about'
    });
  $urlRouterProvider.otherwise('/home');
}