export default ['$scope', '$http', '$interval', ($scope, $http, $interval) => {
  // dynamically scale width of images to users window
  const windowWidth = window.screen.width;
  // how many images to show in carousel
  const imageIds = [...Array(3).keys()];
  // interval promise
  let promise;

  $scope.homeCarouselInterval = 5000;
  $scope.noWrapSlides = false;
  $scope.active = 0;

  // load random images
  let slides = $scope.slides = [];
  for (const imageId of imageIds) {
    slides.push({
      image: `//unsplash.it/${windowWidth + slides.length + 1}/300/?random`,
      id: imageId
    });
  }

  // Get some jokes - double the interval time (don't want to be rude)
  $scope.getJoke = () => {
    $http
      .get('//api.icndb.com/jokes/random?limitTo=[nerdy]')
      .then((response) => {
        const joke = response.data.value.joke;

        // Hack to decode reponse
        const elem = document.createElement('textarea');
        elem.innerHTML = joke;
        const decoded = elem.value;

        $scope.joke = decoded;
      });
  }
  $scope.getJoke();

  // Stop interval
  $scope.stop = () => {
    $interval.cancel(promise);
  };

  // Start interval
  $scope.start = () => {
    $scope.stop();
    promise = $interval($scope.getJoke, $scope.homeCarouselInterval * 2);
  };

  // When controllor no longer being used, stop interval
  $scope.start();
  $scope.$on('$destroy', () => {
    $scope.stop();
  });
}];
