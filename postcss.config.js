module.exports = {
  plugins: [
    require('autoprefixer')({ browsers: 'ie >= 11, Firefox >= 40, Chrome >= 43, ios >= 8' }),
  ],
};